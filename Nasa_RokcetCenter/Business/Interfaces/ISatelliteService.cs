﻿using DTO;
using System.Collections.Generic;

namespace Business.Interfaces
{
    public interface ISatelliteService
    {
        List<Satellite> GetSatellitebyRocket(int rocketID);
        Satellite GetSatellite(int satelliteID);
        bool CreateSatellite(Satellite satellite);
        bool RemoveSatellite(int satelliteID);
    }
}
