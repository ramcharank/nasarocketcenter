﻿using DTO;
using System.Collections.Generic;

namespace Business.Interfaces
{
    public interface IRocketService
    {
        List<CarrierRocket> GetRockets();
        CarrierRocket GetRocketbyName(int rocketName);
        int CreateRocket(CarrierRocket rocket);
        bool UpdateRocket(int rocketID, string Destination);
        bool LaunchRocket(int rocketID);
    }
}
