﻿using Business.Interfaces;
using DTO;
using System.Collections.Generic;
using System.Linq;

namespace Business
{
    public class SatelliteService : ISatelliteService
    {
        public SatelliteService()
        {

        }
        public List<Satellite> GetSatellitebyRocket(int rocketID)
        {
            return MockData.Satellites.FindAll(satellite => satellite.RocketID == rocketID);
        }

        public Satellite GetSatellite(int satelliteID)
        {
            return MockData.Satellites.Where(satellite => satellite.SatelliteID == satelliteID).FirstOrDefault();
        }

        public bool CreateSatellite(Satellite satellite)
        {
            satellite.SatelliteID = MockData.Satellites.Count + 1;
            MockData.Satellites.Add(satellite);
            return true;
        }

        public bool RemoveSatellite(int satelliteID)
        {
            var satelliteObj = MockData.Satellites.Where(satellite => satellite.SatelliteID == satelliteID).FirstOrDefault();
            MockData.Satellites.Remove(satelliteObj);
            return true;
        }
    }
}
