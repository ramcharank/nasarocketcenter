﻿using Business.Interfaces;
using DTO;
using System.Collections.Generic;
using System.Linq;

namespace Business
{
    public class RocketService : IRocketService
    {
        private ISatelliteService satelliteService;
        public RocketService()
        {
            satelliteService = new SatelliteService();
        }
        public CarrierRocket GetRocketbyName(int rocketID)
        {
            return MockData.Rockets.Where(rocket => rocket.RocketID == rocketID).FirstOrDefault();
        }

        public List<CarrierRocket> GetRockets()
        {
            return MockData.Rockets;
        }
        public int CreateRocket(CarrierRocket rocket)
        {
            rocket.RocketID = MockData.Rockets.Count + 1;
            MockData.Rockets.Add(rocket);
            return rocket.RocketID;
        }

        public bool UpdateRocket(int rocketID, string destination)
        {
            MockData.Rockets.Where(rocket => rocket.RocketID == rocketID).FirstOrDefault().Destination = destination;
            return true;
        }

        public bool LaunchRocket(int rocketID)
        {
            return true;
        }
    }
}
