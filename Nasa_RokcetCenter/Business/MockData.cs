﻿using DTO;
using System.Collections.Generic;

namespace Business
{
    public static class MockData
    {
        public static List<CarrierRocket> Rockets = new List<CarrierRocket>()
        {
            new CarrierRocket(){ RocketID = 1, RocketName = "Sputhnik1", Destination = "Moon"},
            new CarrierRocket(){ RocketID = 2, RocketName = "Curiosity", Destination = "Mars"},
            new CarrierRocket(){ RocketID = 3, RocketName = "Voyager", Destination = "Orbit"}
        };
        public static List<Satellite> Satellites = new List<Satellite>()
        {
            new Satellite(){ SatelliteID = 1, SatelliteName = "Scrutinity", RocketID = 1, SatelliteCategory = SatelliteTypes.Surveillance},
            new Satellite(){ SatelliteID = 2, SatelliteName = "Vulture", RocketID = 1, SatelliteCategory = SatelliteTypes.Surveillance},
            new Satellite(){ SatelliteID = 3, SatelliteName = "Charlie", RocketID = 1, SatelliteCategory = SatelliteTypes.Surveillance},
            new Satellite(){ SatelliteID = 4, SatelliteName = "Storm", RocketID = 2, SatelliteCategory = SatelliteTypes.Weather},
            new Satellite(){ SatelliteID = 5, SatelliteName = "Bravo", RocketID = 2, SatelliteCategory = SatelliteTypes.Weather},
            new Satellite(){ SatelliteID = 6, SatelliteName = "Blue", RocketID = 3, SatelliteCategory = SatelliteTypes.Maps},
            new Satellite(){ SatelliteID = 7, SatelliteName = "Alfa", RocketID = 3, SatelliteCategory = SatelliteTypes.Maps},
            new Satellite(){ SatelliteID = 8, SatelliteName = "Bravo", RocketID = 3, SatelliteCategory = SatelliteTypes.Maps}
        };       
        
    }
}
