﻿using System.Collections.Generic;

namespace DTO
{
    public class CarrierRocket : Rocket
    {
        public List<Satellite> Satellites { get; set; }      
    }
}
