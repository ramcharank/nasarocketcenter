﻿namespace DTO
{
    public class Rocket
    {
        public int RocketID { get; set; }
        public string RocketName { get; set; }
        public string Destination { get; set; }
    }
}
