﻿namespace DTO
{
    public class Satellite
    {
        public int SatelliteID { get; set; }
        public string SatelliteName { get; set; }
        public int RocketID { get; set; }
        public SatelliteTypes SatelliteCategory { get; set; }
    }
}
