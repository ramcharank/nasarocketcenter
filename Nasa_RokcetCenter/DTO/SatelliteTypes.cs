﻿namespace DTO
{
    public enum SatelliteTypes
    {
        Weather = 1,
        Maps = 2,
        Surveillance = 3
    }
}
