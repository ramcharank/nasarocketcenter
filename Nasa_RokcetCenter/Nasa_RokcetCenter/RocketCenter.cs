﻿using Business.Interfaces;
using DTO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Nasa_RokcetCenter
{
    class RocketCenter
    {
        private static IRocketService _rocketService;
        private static ISatelliteService _satelliteService;
        public RocketCenter(IRocketService rocketService, ISatelliteService satelliteService)
        {
            _rocketService = rocketService;
            _satelliteService = satelliteService;
        }

        public void Run()
        {
            List<CarrierRocket> rockets = _rocketService.GetRockets();
            Console.WriteLine("NASA ROCKET CENTER");
            //rockets = rocketService.GetRockets();
            foreach (var rocket in rockets)
            {
                Console.WriteLine(rocket.RocketID + ". " + rocket.RocketName);
            }
            Console.WriteLine("Enter ID of the Rocket to select or Press C to create a new Rocket");
            var rocketID = Console.ReadLine();
            if (rocketID.ToUpper() == "C")
            {
                CarrierRocket rocket = new CarrierRocket();
                Console.WriteLine("Enter Rocket Name");
                rocket.RocketName = Console.ReadLine();
                Console.WriteLine("Enter Destination Path");
                rocket.Destination = Console.ReadLine();
                var rktID = _rocketService.CreateRocket(rocket);
                Console.WriteLine("Rocket created successfully");
                Console.WriteLine("Do you wish to Add Satellites? Y or N");
                var readKey = Console.ReadLine();
                if(readKey.ToUpper() == "Y")
                {
                    CreateSatellite(rktID);
                }
            }
            else if (rockets.FindAll(rocket => rocket.RocketID == Convert.ToInt32(rocketID)).Count > 0)
            {
                Console.WriteLine("Please select an option");
                Console.WriteLine("1. Show Satellites");
                Console.WriteLine("2. Update Destination");
                Console.WriteLine("3. Launch Rocket");
                Console.WriteLine("4. Show Satellites by Category");
                var option = Console.ReadLine();
                switch (option)
                {
                    case "1":
                        SatelliteManager(Convert.ToInt32(rocketID));
                        break;
                    case "2":
                        UpdateRocket(Convert.ToInt32(rocketID));
                        break;
                    case "3":                        
                        LaunchRocket(Convert.ToInt32(rocketID));
                        break;
                    case "4":
                        ShowSatellitesbyCategory(Convert.ToInt32(rocketID));
                        break;
                }
            }
            Console.WriteLine("Press any Key to exit");
            Console.ReadKey();
        }
        private static void SatelliteManager(int rocketID)
        {
            var satellites = _satelliteService.GetSatellitebyRocket(rocketID);
            foreach (var satellite in satellites)
            {
                Console.WriteLine(satellite.SatelliteID + ". " + satellite.SatelliteName);
            }
            Console.WriteLine("Enter ID of the Satellite to remove from Rocket or Press C to Add a new Satellite");
            var satID = Console.ReadLine();
            if (satID.ToUpper() == "C")
            {
                CreateSatellite(rocketID);
            }
            else if (satellites.FindAll(s => s.SatelliteID == Convert.ToInt32(satID)).Count > 0)
            {
                _satelliteService.RemoveSatellite(Convert.ToInt32(satID));                
            }
        }
        private static void CreateSatellite(int rocketID)
        {
            Satellite satellite = new Satellite();
            satellite.RocketID = Convert.ToInt32(rocketID);
            Console.WriteLine("Enter Satellite Name");
            satellite.SatelliteName = Console.ReadLine();
            Console.WriteLine("Enter Number to select the Category from below");
            Console.WriteLine("1. Weather || 2. Maps || 3. Surveillance");
            satellite.SatelliteCategory = (SatelliteTypes)(Convert.ToInt32(Console.ReadLine()));
            var result = _satelliteService.CreateSatellite(satellite);
            if (result)
            {
                Console.WriteLine("Satellite Created Successfully");
            }
        }
        private static void UpdateRocket(int rocketID)
        {
            Console.WriteLine("Ënter Destination");
            var destination = Console.ReadLine();
            var result = _rocketService.UpdateRocket(Convert.ToInt32(rocketID), destination);
            if (result)
            {
                Console.WriteLine("Updated Successfully");
            }
        }
        private static void ShowSatellitesbyCategory(int rocketID)
        {
            var satellites = _satelliteService.GetSatellitebyRocket(rocketID);
            satellites.OrderBy(sat => sat.SatelliteCategory);
            foreach (var satellite in satellites)
            {
                Console.WriteLine(satellite.SatelliteCategory + " - " + satellite.SatelliteName);
            }
        }
        private static void LaunchRocket(int rocketID)
        {
            var result = _rocketService.LaunchRocket(Convert.ToInt32(rocketID));
            if (result)
            {
                Console.WriteLine("Rocket Launched Successfully");
            }
        }
    }
}
