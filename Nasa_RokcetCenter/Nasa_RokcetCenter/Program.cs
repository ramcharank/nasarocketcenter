﻿using Business;
using Business.Interfaces;
using Microsoft.Practices.Unity;

namespace Nasa_RokcetCenter
{
    class Program
    {        
        static void Main(string[] args)
        {
            IUnityContainer container = new UnityContainer();
            container.RegisterType<IRocketService, RocketService>();
            container.RegisterType<ISatelliteService, SatelliteService>();

            var rocketCenter = container.Resolve<RocketCenter>();
            rocketCenter.Run();
        }        
    }
}
